export interface PredictModel {
  Typeroute: string;
  Speed: string;
  Latitude: string;
  Longitude: string;
  Date: any;
  Predicted: string;
  TypeAI?: string;
}
