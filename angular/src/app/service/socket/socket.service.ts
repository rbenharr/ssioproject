import {Injectable} from '@angular/core';
import {Socket} from 'ngx-socket-io';
import {Observable} from "rxjs";
import 'rxjs/add/operator/map';
import {PredictModel} from "../../models/predict.model";

@Injectable({
  providedIn: 'root'
})
export class SocketService {

  constructor(private socket: Socket) {}

  public receiveData(): Observable<PredictModel[]> {
    return this.socket.fromEvent<any>('test').map((data:any) => JSON.parse(data));
  }
}
