import {Component, Input, OnInit} from '@angular/core';
import {icon, LatLngLiteral, map, Marker, marker, tileLayer, Map} from 'leaflet';
import {SocketService} from "../service/socket/socket.service";
import {Observable} from "rxjs";
import {PredictModel} from "../models/predict.model";

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {

  informations: Observable<PredictModel[]>;
  markers: Marker[] = [];
  mapRealTime: Map;

  @Input()
  coordinates: LatLngLiteral = {
    lat: 48.8534,
    lng: 2.3488
  };

  @Input()
  title = 'Grahntech';


  constructor(private socketService: SocketService) {
    this.informations = this.socketService.receiveData();
  }

  ngOnInit() {
    this.mapRealTime = map('map').setView(this.coordinates, 12);

    tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
      attribution: this.title
    }).addTo(this.mapRealTime);


    this.informations.subscribe((data: PredictModel[]) => {
      this.clearMarkers();
      data.forEach((v: PredictModel) => {
        this.createMarkers({'lat': +v.Latitude, 'lng': +v.Longitude}, '<dl><dt>Speed : ' + v.Speed + 'km/h </dt><dt> Date : ' + new Date(v.Date) + '</dt>Type road : ' + v.Typeroute+'</dl>', v.Predicted, v.TypeAI)
      });
      console.log(this.markers);
      // data.forEach((v:PredictModel) => console.log(v));
      console.log(data);
    });
  }


  private createMarkers(info: LatLngLiteral, text: string, type: string, aiType: string) {
    let url = 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.2.0/images/marker-icon.png';

    if(type === 'walker' && aiType === 'knn') {
      url = 'assets/img/pietonKNN.png'
    } else if(type === 'car' && aiType === 'knn'){
      url='assets/img/voitureKNN.png'
    } else if (type == 'bike' && aiType === 'knn') {
      url = 'asserts/img/veloKNN.png'
    } else if(type === 'walker' && aiType === 'tree'){
      url='assets/img/pietonT.png'
    } else if (type == 'car' && aiType === 'tree') {
      url='asserts/img/voitureT.png'
    } else if (type == 'bike' && aiType === 'tree') {
      url='asserts/img/veloT.png'
    }

    const myIcon = icon({
      iconUrl: url
    });
    const m = marker(info, {icon: myIcon}).bindPopup(text).addTo(this.mapRealTime).openPopup();
    this.markers.push(m);
  }


  private clearMarkers() {
    this.markers.forEach(marker => this.mapRealTime.removeLayer(marker));
  }
}
