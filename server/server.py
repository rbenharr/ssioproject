#Lib server
from flask import Flask, request, jsonify, abort
from flask_pymongo import PyMongo
import datetime
from flask_socketio import SocketIO, send, emit

# pip install geopy
from geopy.geocoders import Nominatim

# pip install requests
import requests
import json
from jsonmerge import merge

#Lib IA
from sklearn.neighbors import KNeighborsClassifier
import mglearn
import matplotlib.pyplot as plt
from sklearn.datasets import load_breast_cancer
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
from sklearn import tree

#Lib IA2
import subprocess
import pandas as pd
import numpy as np
from sklearn.tree import DecisionTreeRegressor  

  

app = Flask(__name__)

app.config['MONGO_DBNAME'] = 'grahntech'
app.config['MONGO_URI'] = 'mongodb://localhost:27017/grahntech'
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app)
mongo = PyMongo(app)

#########################
###### PARTIE IA ########
#########################

def IA(typeroute, speed, latitude, longitude):

  mycol = mongo.db.predict
  data={}
  data['Typeroute'] = parseCoords(latitude, longitude)
  data['Speed'] = speed
  data['Latitude'] = latitude
  data['Longitude'] = longitude
  data['Date'] = datetime.datetime.now()
  data['TypeAI'] = "knn"

  print(typeroute)
  print(speed)
  maxSpeed = []
  vehicle = []
  routeType = []

  getDataForAI(maxSpeed, vehicle, routeType)

  if(len(maxSpeed) < 24) :
     # First Feature
    maxSpeed=[135,148,155,115,123,8,13,5,4,7,30,5,7,8,9,10,30,20,40,70,62,48,2,67]
    # Second Feature
    routeType=[200,200,200,200,200,0,0,0,0,0,200,200,200,200,200,200,200,200,200,200,200,200,200,200]

    #200 = highway, 0 = amenity, 1 = building, 2 = historic, 3 = landuse
    #180 = cycleway, 40 = pedestrian, 30 = path

    # Label or target varible
    vehicle=['Voiture','Voiture','Voiture','Voiture','Voiture','Walk','Walk','Walk','Walk','Walk','Voiture','Walk','Walk','Walk','Walk','Walk','Voiture','Voiture','Voiture','Voiture','Voiture','Voiture','Voiture','Voiture'] 

  #creating labelEncoder
  le = preprocessing.LabelEncoder()
  # Converting string labels into numbers.
  routeType_encoded= routeType
  maxSpeed_encoded = maxSpeed
  features=list(zip(routeType_encoded,maxSpeed_encoded))
  label=le.fit_transform(vehicle)
  model = KNeighborsClassifier(n_neighbors=3)

  # Train the model using the training sets
  model.fit(features,label)

  #Predict Output
  predicted= model.predict([[typeroute,speed]])

  if predicted == 0:
    data['Predicted'] = "car"
    if(mycol.insert_one(data)):
      handle_json()
    return "car"
  else:
    data['Predicted'] = "walker"
    if(mycol.insert_one(data)):
      handle_json()
    return "walker"

  return "none"

def IATree(typeroute, speed, latitude, longitude):
  mycol = mongo.db.predict
  data={}
  data['Typeroute'] = parseCoords(latitude, longitude)
  data['Speed'] = speed
  data['Latitude'] = latitude
  data['Longitude'] = longitude
  data['Date'] = datetime.datetime.now()
  data['TypeAI'] = "tree"

  maxSpeed = []
  vehicle = []
  routeType = []

  dataSpeedRoute = []
  dataVehicle = []

  getDataForAI(maxSpeed, vehicle, routeType)

  if(len(maxSpeed) < 24) :
    dataSpeedRoute = [[135,200],
    [148,200],
    [155,200],
    [115,200],
    [123,200],
    [8,0],
    [13,0],
    [5,0],
    [4,0],
    [7,0],
    [30,200],
    [5,0],
    [7,0],
    [8,0],
    [9,0],
    [10,0],
    [30,200],
    [20,200],
    [40,200],
    [70,200],
    [62,200],
    [48,200],
    [2,200],
    [67,200],
    [3,200],
    [4,200],
    [5,200],
    [6,200],
    [7,200]]
    dataVehicle = [
      [0], 
      [0], 
      [0], 
      [0],
      [0],
      [1],
      [1],
      [1],
      [1],
      [1],
      [0],
      [1],
      [1],
      [1],
      [1],
      [1],
      [0],
      [0],
      [0],
      [0],
      [0],
      [0],
      [1],
      [0],
      [1],
      [1],
      [1],
      [1],
      [1]
      ]
  else:
    for a in maxSpeed :
      for b in routeType :
        dataSpeedRoute.append([a,b])
    for c in vehicle :
      dataVehicle.append([c])

  dataset = np.array(dataSpeedRoute)
  labels = np.array(dataVehicle)

  # select all rows by : and column 1 
  # by 1:2 representing features 
  routeType = dataset[:, 1:2].astype(float) 

  maxSpeed = dataset[:,0].astype(float)
    
  regressor = DecisionTreeRegressor(random_state = 0)  
    
  regressor.fit(dataset,labels)

  predicted = regressor.predict([[speed,typeroute]])

  if predicted == 0:
    data['Predicted'] = "car"
    if(mycol.insert_one(data)):
      handle_json()
    return "car"
  else:
    data['Predicted'] = "walker"
    if(mycol.insert_one(data)):
      handle_json()
    return "walker"

  return "none"

####POUR KNN####
@app.route('/knn', methods=['POST']) 
def comIAKNN():
    if(request.get_json() is False):
      print("Error get json")
      abort(400)
    content = request.get_json()

    try:
        speed = content['Vitesse']
        longitude = content['Longitude']
        latitude = content['Latitude']
    except Exception as e:
      print("Exception comIA function")
      print(e)
      abort(400)

    typeroute = parseCoords(latitude, longitude)

    return IA(getNumberTypeRoute(typeroute), speed, latitude, longitude)

####POUR DT####
@app.route('/dt', methods=['POST']) 
def comIADT():
    if(request.get_json() is False):
      print("Error get json")
      abort(400)
    content = request.get_json()

    try:
        speed = content['Vitesse']
        longitude = content['Longitude']
        latitude = content['Latitude']
    except Exception as e:
      print("Exception comIA function")
      print(e)
      abort(400)

    typeroute = parseCoords(latitude, longitude)

    return IATree(getNumberTypeRoute(typeroute), speed, latitude, longitude)

def getNumberTypeRoute(typeroute):
  with open("config_buildings.json") as f:
    data = json.loads(f.read())
  try:
    return data[typeroute]
  except Exception as e:
    print("Exception getNumberTypeRoute function, please add the following road into config_buildings.json : ")
    print(e)
    abort(400)

def getDataForAI(vitesse_max, typeVehicule, routeType):
  vitesse=-1
  route=-1
  vehicule=-1
  id=-1

  for data in mongo.db.vehicules.find():
    if data.get("ID") != id:
      if id!= -1:
        vitesse_max.append(vitesse)
        typeVehicule.append(vehicule)
        routeType.append(int(getNumberTypeRoute(route)))

      id = data.get("ID")
      vitesse = float(data.get("Vitesse"))
      route = data.get("routeType")
      vehicule = data.get("Vehicule")
    else:
      if vitesse > float(data.get("Vitesse")):
        vitesse = float(data.get("Vitesse"))

  print(vitesse_max)
  print(typeVehicule)
  print(routeType)

  return "ok"


##############################
####### PARTIE SERVEUR #######
##############################

@socketio.on('connect')
def handle_json():
  mycol = mongo.db.predict

  print('on envoie')
  socketio.emit('test',json.dumps(list(mycol.find({}, {'_id': False})), indent=4, sort_keys=True, default=str), json=True)

    
def parseCoords(latitude, Longitude):

      url = "https://nominatim.openstreetmap.org/reverse"
      lat = latitude
      lon = Longitude
      payload = {'format':'jsonv2', 'lat':lat, 'lon':lon}
      resp = requests.post(url, params=payload  )

      if resp.ok:
          info = json.loads(resp.text)
          category = info['category']
          type_device = info['type']
          print(category)
          return category
      else:
          print ("Error parse coord: (lat: %f,long: %f)" %(lat, long))

def addJson():

  data = request.get_json()
  vehicule = data['Vehicule']
  latitude = data['Latitude']
  longitude = data['Longitude']
  vitesse = data['Vitesse']


  return jsonify({"Vehicule" : vehicule, "Latitude" : latitude, "Longitude" : longitude, "Vitesse" : vitesse, "category" : parseCoords(latitude, longitude)})


@app.route('/', methods=['GET', 'POST'])
def index():

    target = os.path.join(app_root, 'static/img/')

    if not os.path.isdir(target):
        os.makedirs(target)
        file = request.files['u_img']
        file_name = file.filename or ''
        destination = '/'.join([target, file_name])
        file.save(destination)

    return render_template('index.html')


@app.route('/receive', methods=['POST'])
def receive():

    mycol = mongo.db.vehicules
    data = request.get_json()
    vitesse = data['Vitesse']
    latitude = data['Latitude']
    longitude = data['Longitude']
    vehicule = data['Vehicule']
    data['routeType'] = parseCoords(latitude,longitude)
    
    if(mycol.insert_one(data)):
        return jsonify({"Vehicule" : str(request.get_json())}), 200, {'ContentType': 'application/json'}
    
    abort(500)



if __name__ == '__main__':
    socketio.run(app, port=5000)
