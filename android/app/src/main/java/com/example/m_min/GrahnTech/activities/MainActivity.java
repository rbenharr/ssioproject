package com.example.m_min.GrahnTech.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.m_min.GrahnTech.async.PostTask;
import com.example.m_min.GrahnTech.R;
import com.example.m_min.GrahnTech.utils.Settings;
import com.example.m_min.GrahnTech.webSocket.MessageListener;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    private Handler handler;
    private FusedLocationProviderClient mFusedLocationClient;
    private int MY_PERMISSIONS_REQUEST_FINE_LOCATION;
    private int MY_PERMISSIONS_REQUEST_COARSE_LOCATION;
    private String[] vehicule = {""};
    private AppLocationManager app;
    private JSONGenerator jsong;
    private String ID;
    private MessageListener messList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        messList = new MessageListener(this);

        Button send = findViewById(R.id.accept);
        RadioGroup group = findViewById(R.id.RGroup);

        TextView dataText = findViewById(R.id.data);
        Button changeActivity = findViewById(R.id.ia);
        vehicule[0] = "Walk";
        //Demande des autorisations

        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION) && ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_FINE_LOCATION);

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_COARSE_LOCATION);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            // Permission has already been granted
        }

        RadioButton pieton = (RadioButton) findViewById(R.id.pieton);
        RadioButton velo = (RadioButton) findViewById(R.id.velo);
        RadioButton voiture = (RadioButton) findViewById(R.id.voiture);

        app = new AppLocationManager(this, dataText);
        jsong = new JSONGenerator();
        ID = UUID.randomUUID().toString();

        pieton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                vehicule[0] = "Walk";
                pieton.setChecked(true);
                velo.setChecked(false);
                voiture.setChecked(false);
            }
        });

        velo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                vehicule[0] = "Bike";
                pieton.setChecked(false);
                velo.setChecked(true);
                voiture.setChecked(false);
            }
        });

        voiture.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                vehicule[0] = "Car";
                pieton.setChecked(false);
                velo.setChecked(false);
                voiture.setChecked(true);
            }
        });


        group.setOnCheckedChangeListener((radioGroup, i) -> {

            //Choix du type de véhicule selon le bouton pressé
            switch (i) {
                case R.id.pieton:
                    System.out.println("pieton");
                    vehicule[0] = "Walk";
                    break;
                case R.id.velo:
                    System.out.println("velo");
                    vehicule[0] = "Bike";
                    break;
                case R.id.voiture:
                    System.out.println("voiture");
                    vehicule[0] = "Car";
                    break;

            }
        });

        handler = new Handler();

//        Notification notification = new Notification(this);
//        notification.createNotificationChannel();
//
//        startService(new Intent(this, MessageNotificationService.class));

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);


        send.setTag(1);

        //Envoi des données si le bouton envoyer est pressé
        send.setOnClickListener((view) -> {
            if((Integer) view.getTag() == 1){
                handler.postDelayed( new Runnable() {
                                         public void run() {
                                             sendDataFct(dataText);
                                             handler.postDelayed(this, 1000);
                                         }
                                     }
                        , 1000);
                send.setTag(0);
                send.setText("Stop send data");
            } else {
                handler.removeCallbacksAndMessages(null);
                send.setText("Send data");
                send.setTag(1);
            }
        });

        changeActivity.setOnClickListener((view) -> {

            Intent myIntent = new Intent(this, Main2Activity.class);
            startActivity(myIntent);

        });
    }

    public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();

        switch (view.getId()) {
            case R.id.pieton:
//                if(checked)
//                    vehicule = "Walk";
                break;
            case R.id.velo:
//                if(checked)
//                    return "Bike";
                break;
            case R.id.voiture:
//                if(checked)
//                    return "Car";
                break;

        }
    }

    @SuppressLint("MissingPermission")
    public void sendDataFct(TextView dataText) {
        Map<String, String> data;
        data = jsong.getJSON(ID,vehicule[0],app);
        dataText.setText(vehicule[0] + " | " + data.get("Vitesse") + " | " + data.get("Longitude") + " | " + data.get("Latitude"));
        String promisJeChangeCa = data.get("Longitude");
        if(!promisJeChangeCa.equals("Calcul...")) {
            try {
                new PostTask(Settings.loadSettings(this), "receive", dataText).execute(new Map[]{data});
            } catch (IOException e) {
                System.out.println("aaaa");
                e.printStackTrace();
            } catch (URISyntaxException e) {
                System.out.println("bbbbb");
                e.printStackTrace();
            }
        }
    }





}
