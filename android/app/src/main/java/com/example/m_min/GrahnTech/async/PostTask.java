package com.example.m_min.GrahnTech.async;

import android.os.AsyncTask;
import android.widget.TextView;

import com.example.m_min.GrahnTech.utils.Post;
import com.example.m_min.GrahnTech.utils.Settings;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;


public class PostTask extends AsyncTask<Map<String, String>, Void, String> {

    private String host;
    private int port;
    private String path;
    private TextView dataText;

    public PostTask(Settings settings, String path, TextView result) throws URISyntaxException {
        URI uri = new URI(settings.getUrl());
        host = uri.getHost();
        port = uri.getPort();
        this.path = path;
        this.dataText = result;
    }


    @Override
    protected String doInBackground(Map<String, String>... maps) {
        try {
            System.out.println(port);
            String prediction = Post.post(host, port, path, maps[0]);
            if(path.equals("knn") || path.equals("dt")) dataText.setText(prediction);
            return prediction;
        } catch (IOException e) {
            System.out.println(e);
            return null;
        }

    }
}
