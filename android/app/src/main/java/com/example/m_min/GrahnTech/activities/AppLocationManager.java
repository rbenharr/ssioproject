package com.example.m_min.GrahnTech.activities;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.widget.TextView;

import java.util.Calendar;

public class AppLocationManager implements LocationListener {

    private LocationManager locationManager;
    private double latitude;
    private double longitude;
    private double speed;
    private Criteria criteria;
    private String provider;

    private double maxSpeed = 0;
    private double maxLat;
    private double maxLon;

    private Location lastKnownLoc;

    private long lastChange;
    private TextView dataText;

    public AppLocationManager(Context context, TextView tv) {
        locationManager = (LocationManager) context
                .getSystemService(Context.LOCATION_SERVICE);
        criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        provider = locationManager.getBestProvider(criteria, true);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000,
                0, this);
        setMostRecentLocation(locationManager.getLastKnownLocation(provider));

        lastChange = Calendar.getInstance().getTimeInMillis();

        dataText = tv;
    }

    private void setMostRecentLocation(Location lastKnownLocation) {
    }

    public String getLatitude() {
        return String.valueOf(latitude);
    }

    public String getLongitude() {
        return String.valueOf(longitude);
    }

    public String getSpeed(){
        return String.valueOf(speed);
    }

    public String getMaxLat() {

        return String.valueOf(maxLat);
    }

    public String getMaxLon() {

        return String.valueOf(maxLon);
    }

    public String getMaxSpeed(){
        return String.valueOf(maxSpeed);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * android.location.LocationListener#onLocationChanged(android.location.
     * Location)
     */
    @Override
    public void onLocationChanged(Location location) {

        if (location != null) {

            double newSpeed = location.getSpeed() * 3.6;
            if(newSpeed > this.maxSpeed){
                maxSpeed = newSpeed;
                maxLat = location.getLatitude();
                maxLon = location.getLongitude();
            }

            this.lastKnownLoc = location;
            this.latitude = location.getLatitude();
            this.longitude = location.getLongitude();
            this.speed = newSpeed;

        }

    }

    /*
     * (non-Javadoc)
     *
     * @see
     * android.location.LocationListener#onProviderDisabled(java.lang.String)
     */
    @Override
    public void onProviderDisabled(String arg0) {
        // TODO Auto-generated method stub

    }

    /*
     * (non-Javadoc)
     *
     * @see
     * android.location.LocationListener#onProviderEnabled(java.lang.String)
     */
    @Override
    public void onProviderEnabled(String arg0) {
        // TODO Auto-generated method stub

    }

    /*
     * (non-Javadoc)
     *
     * @see android.location.LocationListener#onStatusChanged(java.lang.String,
     * int, android.os.Bundle)
     */
    @Override
    public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
        // TODO Auto-generated method stub

    }

}
