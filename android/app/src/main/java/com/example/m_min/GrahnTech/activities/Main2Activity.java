package com.example.m_min.GrahnTech.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.m_min.GrahnTech.async.PostTask;
import com.example.m_min.GrahnTech.R;
import com.example.m_min.GrahnTech.utils.Settings;
import com.example.m_min.GrahnTech.webSocket.MessageListener;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class Main2Activity extends AppCompatActivity {

    private Handler handler;
    private FusedLocationProviderClient mFusedLocationClient;
    private int MY_PERMISSIONS_REQUEST_FINE_LOCATION;
    private int MY_PERMISSIONS_REQUEST_COARSE_LOCATION;
    private AppLocationManager app;
    private JSONGenerator jsong;
    private String ID;
    private MessageListener messList;
    private String path;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        messList = new MessageListener(this);
        path = "ai";

        Button send = findViewById(R.id.accept);
        Button changeActivity = findViewById(R.id.datasend);

        TextView dataText = findViewById(R.id.data);

        Spinner aiChoice = findViewById(R.id.ia);

        List choices = new ArrayList();
        choices.add("KNN");
        choices.add("Decision Tree");

        ArrayAdapter adapter = new ArrayAdapter(
                this,
                android.R.layout.simple_spinner_item,
                choices
        );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        aiChoice.setAdapter(adapter);

        aiChoice.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                String item = parent.getItemAtPosition(pos).toString();

                // Showing selected spinner item
                if(item.equals("KNN")){
                    path = "knn";
                    Toast.makeText(parent.getContext(), "Selected: KNN", Toast.LENGTH_LONG).show();
                }
                else if(item.equals("Decision Tree")){
                    path = "dt";
                    Toast.makeText(parent.getContext(), "Selected: DT", Toast.LENGTH_LONG).show();
                }
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        //Demande des autorisations

        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION) && ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_FINE_LOCATION);

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_COARSE_LOCATION);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            // Permission has already been granted
        }

        app = new AppLocationManager(this, dataText);
        jsong = new JSONGenerator();
        ID = UUID.randomUUID().toString();

        handler = new Handler();

        send.setTag(1);

        //Envoi des données si le bouton envoyer est pressé
        send.setOnClickListener((view) -> {
            if((Integer) view.getTag() == 1){
                send.setTag(0);
                send.setText("Predict");
            } else {
                sendDataFct(dataText);
                send.setText("Prediction Done");
            }
        });

        changeActivity.setOnClickListener((view) -> {

            Intent myIntent = new Intent(this, MainActivity.class);
            startActivity(myIntent);

        });

    }

    @SuppressLint("MissingPermission")
    public void sendDataFct(TextView dataText) {
        Map<String, String> data;
        data = jsong.getJSONPredict(app);
        dataText.setText(data.get("Vitesse") + " | " + data.get("Longitude") + " | " + data.get("Latitude"));
        String promisJeChangeCa = data.get("Longitude");
        if(!promisJeChangeCa.equals("Calcul...")) {
            try {
                new PostTask(Settings.loadSettings(this), path, dataText).execute(new Map[]{data});
            } catch (IOException e) {
                System.out.println("aaaa");
                e.printStackTrace();
            } catch (URISyntaxException e) {
                System.out.println("bbbbb");
                e.printStackTrace();
            }
            finally{
                //Attente de la réponse ?
            }
        }
    }





}
