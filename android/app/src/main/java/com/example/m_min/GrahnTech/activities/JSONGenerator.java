package com.example.m_min.GrahnTech.activities;

import android.support.v7.app.AppCompatActivity;

import java.util.HashMap;
import java.util.Map;

public class JSONGenerator extends AppCompatActivity {

    public Map<String,String> getJSON(String id, String vehicule, AppLocationManager alm) {

        Map<String, String> data = new HashMap<>();
        data.put("ID",id);
        data.put("Vehicule",vehicule);

        String latitude = alm.getLatitude();
        String longitude = alm.getLongitude();
        String speed = alm.getSpeed();
        if(longitude != null && latitude != null){
            data.put("Latitude",latitude);
            data.put("Longitude",longitude);
            data.put("Vitesse",speed);
        }
        else {
            data.put("Latitude","Calcul...");
            data.put("Longitude","Calcul...");
            data.put("Vitesse","Calcul...");
        }


        return data;
    }

    public Map<String,String> getJSONPredict(AppLocationManager alm) {

        Map<String, String> data = new HashMap<>();
        String maxSpeed = alm.getMaxSpeed();
        String maxLat = alm.getMaxLat();
        String maxLon = alm.getMaxLon();
        if(maxLon != null && maxLat != null){
            data.put("Latitude",maxLat);
            data.put("Longitude",maxLon);
            data.put("Vitesse",maxSpeed);
        }
        else {
            data.put("Latitude","Calcul...");
            data.put("Longitude","Calcul...");
            data.put("Vitesse","Calcul...");
        }


        return data;
    }

}
