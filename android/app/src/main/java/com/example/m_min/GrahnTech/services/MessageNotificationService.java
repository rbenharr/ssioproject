package com.example.m_min.GrahnTech.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.example.m_min.GrahnTech.webSocket.MessageListener;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.WebSocket;

public class MessageNotificationService extends Service {

    private OkHttpClient client = new OkHttpClient();
    private WebSocket webSocket;



    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
//        startWatching(Settings.loadSettings(getApplicationContext()).getUrl().replace("http", "ws"));
        webSocket.send("pushSince:android");
        return START_NOT_STICKY;
    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void onDestroy() {
        webSocket.close(1000,null);
    }


    private void startWatching(String url) {
        Request request = new Request.Builder().url(url).build();
        MessageListener listener = new MessageListener(this);
        webSocket = client.newWebSocket(request, listener);
    }

}
